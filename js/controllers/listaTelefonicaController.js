 angular.module("listaTelefonica").controller("listaTelefonicaController",
    function ($scope, $http) {
        // $http: permite requisições utilizando XMLHttpRequest ou via JSONP: get, post, put, delete, head, jsonp
        // body...
        $scope.pageHeader="Phonebook";

        $scope.operators = [
            {description: "Oi", code:1, category:"Mobile"},
            {description: "Tim", code:2, category:"Mobile"},
            {description: "Vivo", code:3, category:"Mobile"},
            {description: "Claro", code:4, category:"Mobile"},
            {description: "Embratel", code:4, category:"Landline"}
        ]

        $scope.contacts = [
            {name: "Anderson Carvalho", phone: "8888-8888", operator:{description: "Oi", code:1, category:"Mobile"}},
            {name: "Rebeka Costa", phone: "9999-9999", operator:{description: "Embratel", code:4, category:"Landline"} },
            {name: "Maria do Socorro", phone: "7777-7777", operator:{description: "Vivo", code:3, category:"Mobile"}}
        ]

        $scope.addContact = function (contact) {
            //  faz uma cópia do objeto recebido e adiciona ao array
            $scope.con
            $scope.contacts.push(angular.copy(contact));
            //  deleta o objeto do escopo
            delete $scope.contact;
        };

        $scope.removeContact = function (contact) {
            //  captura o index do objeto
            index = $scope.contacts.indexOf(contact);
            /**
                utiliza este index para remover
                (splite é o método de remoção)
            */
            $scope.contacts.splice(index,1);
        }

        $scope.cleanView = function () {
            //  deleta o objeto do escopo
            delete $scope.contact;
        }

        $scope.sortingBy = function (field) {
            $scope.ordinationCriterion = field;
            $scope.direction = !$scope.direction;
        };
    });