angular.module("listaTelefonica").directive("uiMessage", function () {
    // body...
    return {
        templateUrl: "../pages/fragmentos/fragmentoMensagem.html",
        replace: true,
        //  restrita a elemento, usada como tag <ui-message>
        restrict: "E",
        transclude: true,
        scope: {
            title: "@message"
        }
    }
});